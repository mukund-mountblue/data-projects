# data.gov.in :: Company Master :: Maharashtra


## Aim
To convert raw open data into charts that tell some kind of story.

## Preparation

#### raw data 

| Name                               | source                                          |
|------------------------------------|-------------------------------------------------|
| Company master data of Maharashtra | https://data.gov.in/catalog/company-master-data |


#### Ancilary data :: Industrial Class

http://mospi.nic.in/classification/national-industrial-classification/alphabetic-index-5digit

**Todo:** upload csv version of this data.


## Instructions

1. Download all the data needed. Consult your mentor if you have any problems
accessiong the raw data.
2. Initialize a node type script project. All your code should be in typescrpt


### Part 1 :: CSV -> JSON
**Important** This is where all your logic will be. Code a node program which
will load the raw csv and convert into a format that can be used to create plots
in part 2.

### Part 2 :: Plot with high charts 
High charts is an open source lib to create plots on the browser. Use the 
json generated previously to plot. Use ajax to load the data onto the 
browser.

## Problems

### 1. Histogram of Authorized Cap

Plot a histogram on AUTHORIZE_CAP with the following intervals
  
  1. < 1L
  2. 1L to 10L
  3. 10L to 1Cr
  4. 10Cr to 100Cr
  5. More than 100Cr
  
Note: You will have to adjust the intervals if you have a un-balanced plot.

### 2. Histogram of company registration by year

From the field DATE_OF_REGISTRATION parse out the registration year. Using
this data, plot a histogram of the number of company registration by year.

### 3. Top registrations by "Principal Business Activity" for the year 2015

In this exercise ... 

  1. only consider registrations for the year 2015
  2. classify and count registrations by PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN
  3. Pick on the top 10 of these values
  4. Plot
  
  
### 4. Top registrations by "Industrial Class"

In this exercise ...
  1. Obtain the industrial class names csv.
  2. count the registrations by INDUSTRIAL_CLASS
  3. Plot top 10 values. The plot should have "Industrial Class" names and not codes
  
### 5. Stacked Bar Chart.

Plot a stacked bar chart by aggregating registrations count over ...
  1. Year of registration
  2. Principal Business Activity
  
  
