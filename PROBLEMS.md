# Source data

https://datahub.io/core/population-growth-estimates-and-projections/r/population-estimates.csv

ASEAN countries list
https://en.wikipedia.org/wiki/ASEAN

SAARC countries list
https://en.wikipedia.org/wiki/South_Asian_Association_for_Regional_Cooperation

# Plots

### 1: India population over years - Bar Chart


### 2: For the year 2015. Bar Chart of population of ASEAN countries

### 3: Over the years, TOTAL population of SAARC countries

In this case for **each year** you have to calculate the sum of the population
of all SAARC countries. Then plot a BAR CHART of **Total SAARC population** vs. year.

### 4: Grouped Bar Chart - ASEAN population vs. years

